let ChangeTheme = document.querySelector(".ChangeTheme");

if(localStorage.getItem("theme") === undefined){
   localStorage.setItem("theme","light");
}

function setTheme(){
document.body.classList.remove("dark");
document.body.classList.remove("light");
document.body.classList.add(localStorage.getItem("theme"));
}

ChangeTheme.addEventListener("click",function(){
   localStorage.setItem("theme",(localStorage.getItem("theme") === "dark") ? "light" : "dark");
   setTheme();
});

setTheme();